﻿namespace DatabaseBackup
{
    partial class ctrlSavePath
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctrlSavePath));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgGoogleDrive = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.imgOneDrive = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.scopeListBox = new System.Windows.Forms.ListBox();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgGoogleDrive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOneDrive)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(13, 121);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(446, 499);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(438, 473);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "One Drive";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(438, 473);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(23, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(42, 42);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // imgGoogleDrive
            // 
            this.imgGoogleDrive.Image = ((System.Drawing.Image)(resources.GetObject("imgGoogleDrive.Image")));
            this.imgGoogleDrive.Location = new System.Drawing.Point(270, 37);
            this.imgGoogleDrive.Name = "imgGoogleDrive";
            this.imgGoogleDrive.Size = new System.Drawing.Size(42, 42);
            this.imgGoogleDrive.TabIndex = 2;
            this.imgGoogleDrive.TabStop = false;
            this.imgGoogleDrive.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(148, 37);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(42, 42);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(209, 37);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(42, 42);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // imgOneDrive
            // 
            this.imgOneDrive.ErrorImage = ((System.Drawing.Image)(resources.GetObject("imgOneDrive.ErrorImage")));
            this.imgOneDrive.Image = ((System.Drawing.Image)(resources.GetObject("imgOneDrive.Image")));
            this.imgOneDrive.Location = new System.Drawing.Point(82, 37);
            this.imgOneDrive.Name = "imgOneDrive";
            this.imgOneDrive.Size = new System.Drawing.Size(42, 42);
            this.imgOneDrive.TabIndex = 5;
            this.imgOneDrive.TabStop = false;
            this.imgOneDrive.Click += new System.EventHandler(this.imgOneDrive_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.imgOneDrive);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.imgGoogleDrive);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(446, 100);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Save to Selected Destinations";
            // 
            // scopeListBox
            // 
            this.scopeListBox.FormattingEnabled = true;
            this.scopeListBox.Items.AddRange(new object[] {
            "wl.signin",
            "wl.basic",
            "onedrive.readonly",
            "onedrive.readwrite",
            "onedrive.appfolder",
            "wl.birthday",
            "wl.calendars",
            "wl.calendars_update",
            "wl.contacts_birthday",
            "wl.contacts_create",
            "wl.contacts_calendars",
            "wl.contacts_photos",
            "wl.contacts_skydrive",
            "wl.emails",
            "wl.events_create",
            "wl.offline_access",
            "wl.phone_numbers",
            "wl.photos",
            "wl.postal_addresses",
            "wl.share",
            "wl.skydrive",
            "wl.skydrive_update",
            "wl.work_profile"});
            this.scopeListBox.Location = new System.Drawing.Point(509, 121);
            this.scopeListBox.Name = "scopeListBox";
            this.scopeListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.scopeListBox.Size = new System.Drawing.Size(13, 121);
            this.scopeListBox.TabIndex = 7;
            // 
            // ctrlSavePath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.scopeListBox);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "ctrlSavePath";
            this.Size = new System.Drawing.Size(477, 636);
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgGoogleDrive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOneDrive)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox imgGoogleDrive;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox imgOneDrive;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox scopeListBox;
    }
}
