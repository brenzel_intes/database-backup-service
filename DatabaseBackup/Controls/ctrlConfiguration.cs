﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using posBang.Library.Helper;
using System.ServiceProcess;

namespace DatabaseBackup
{
    public partial class ctrlConfiguration : UserControl
    {
        public ctrlConfiguration()
        {
            InitializeComponent();
        }

        private void ctrlConfiguration_Load(object sender, EventArgs e)
        {

            try
            {
                SettingFile settings = new SettingFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");

                settings.OpenSection("credentials");
                txtHost.Text = settings.Read<string>("Host", txtHost.Text);
                txtUser.Text = settings.Read<string>("User", txtUser.Text);
                txtPassword.Text = settings.ReadProtect<string>("Password", txtPassword.Text);
                txtPort.Text = settings.Read<string>("Port", txtPort.Text);
                cbDatabase.Text = settings.Read<string>("Database", cbDatabase.Text);
                settings.CloseSection();

                settings.OpenSection("output");
                txtSavePath.Text = settings.Read<string>("SavePath", txtSavePath.Text);              
                settings.CloseSection();              
            }
            catch (Exception ex)
            { }                     
        }

        private void cbDatabase_Enter(object sender, EventArgs e)
        {
            try
            {
                Database.DbHost = txtHost.Text;
                Database.DbUser = txtUser.Text;
                Database.DbPassword = txtPassword.Text;
                Database.DbPort = Convert.ToUInt16(txtPort.Text);
                Database.DbName = cbDatabase.Text;
                Database.Open("");
                DataTable dt = Database.ExecuteQuery("show databases");
                foreach(DataRow row in dt.Rows)
                {
                    cbDatabase.Properties.Items.Add(row[0].ToString());
                }

            }
            catch (Exception ex) { }
        }

        private void txtSavePath_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtSavePath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void txtSavePath_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtSavePath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SettingFile settings = new SettingFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");

                settings.OpenSection("credentials");
                settings.Write("Host", txtHost.Text);
                settings.Write("User", txtUser.Text);
                settings.WriteProtect("Password", txtPassword.Text);
                settings.Write("Port", txtPort.Text);
                settings.Write("Database", cbDatabase.Text);
                settings.CloseSection();

                settings.OpenSection("output");
                settings.Write("SavePath", txtSavePath.Text);              
                settings.CloseSection();

                MessageBox.Show("Configuration Saved!");
                ServiceController service = new ServiceController("DatabaseBackup");
                service.Stop();
                service.Start();
            }
            catch(Exception ex)
            { }

        }

        private void txtPassword_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}
