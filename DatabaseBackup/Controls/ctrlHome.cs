﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using posBang.Library.Helper;
using System.IO;
using MySql.Data.MySqlClient;

namespace DatabaseBackup
{
    public partial class ctrlHome : UserControl
    {
        public ctrlHome()
        {
            InitializeComponent();
        }

        public void LoadSettings()
        {
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini"))
            {


                SettingFile settings = new SettingFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");

                #region Connection
                settings.OpenSection("credentials");
                string host = settings.Read<string>("Host", "");
                string database = settings.Read<string>("Database", "");
                string user = settings.Read<string>("User", "");
                string password = settings.ReadProtect<string>("Password", "");
                string port = settings.Read<string>("Port", "");

                try
                {
                    Database.DbHost = host;
                    Database.DbUser = user;
                    Database.DbPassword = password;
                    Database.DbPort = Convert.ToUInt16(port);
                    Database.DbName = database;
                    Database.Open("");
                    DataTable dt = Database.ExecuteQuery("show databases");

                    lblConnection.Text = host + ":" + port + "/" + database;
                    pbConnection.Visible = true;

                }
                catch (Exception ex)
                {
                    lblConnection.Text = "Not Set";
                    pbConnection.Visible = false;
                }

                settings.CloseSection();

                #endregion

                #region Save Path
                settings.OpenSection("output");
                string savePath = settings.Read<string>("SavePath", "");
                settings.CloseSection();
                if (savePath != "")
                {
                    lblSavePath.Text = savePath;
                    pbSaveTo.Visible = true;
                }
                else
                {
                    lblSavePath.Text = "Not Set";
                    pbSaveTo.Visible = false;
                }

                #endregion

                #region Schedule
                settings.OpenSection("schedule");
                string scheduleType = settings.Read<string>("ScheduleType", "");
                string dayOfWeek = settings.Read<string>("DayOfWeek", "");
                string dayOfMonth = settings.Read<string>("DayOfMonth", "");
                string time = settings.Read<string>("Time", "");

                //DateTime scheduledBackupTime = new DateTime();
                //DateTime.TryParse(time, out scheduledBackupTime);

                if (scheduleType == "Day of Week")
                {
                    lblBackupSchedule.Text = "Start backup every " + dayOfWeek + " at " + time;
                }
                else if (scheduleType == "Day of Month")
                {
                    lblBackupSchedule.Text = "Start backup every " + dayOfMonth + " days of month at " + time;
                }
                else
                {
                    lblBackupSchedule.Text = "Not Set";
                    pbSchedule.Visible = false;
                }
                #endregion
            }
            else
            {
                lblConnection.Text = "Not Set";
                pbConnection.Visible = false;

                lblSavePath.Text = "Not Set";
                pbSaveTo.Visible = false;

                lblBackupSchedule.Text = "Not Set";
                pbSchedule.Visible = false;
            }

        }


        public void readLogs()
        {
            try
            {

                string filepath = AppDomain.CurrentDomain.BaseDirectory + @"\Logs\ServiceLog.txt";
                string[] lines = File.ReadAllLines(filepath);
                dgvLogs.Rows.Clear();
                foreach(string line in lines)
                {
                    string date = line.Remove(line.IndexOf(']')).Substring(line.IndexOf('[') + 1);
                    string description = line.Remove(line.IndexOf('['));
                    dgvLogs.Rows.Add(description, date);
                }
            }
            catch { }

        }

        private void ctrlHome_Load(object sender, EventArgs e)
        {
            


            LoadSettings();
            readLogs();
        }
    }
}
