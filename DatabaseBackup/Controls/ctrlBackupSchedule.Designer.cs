﻿namespace DatabaseBackup
{
    partial class ctrlBackupSchedule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_cntrl = new System.Windows.Forms.TabControl();
            this.tp_dayofWeek = new System.Windows.Forms.TabPage();
            this.clb_dayofWeek = new System.Windows.Forms.CheckedListBox();
            this.chkEveryday = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timeDayofWeek = new DevExpress.XtraEditors.TimeEdit();
            this.tp_dayofMonth = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.chkAllMonth = new System.Windows.Forms.CheckBox();
            this.tlp_calendar = new System.Windows.Forms.TableLayoutPanel();
            this.d28 = new System.Windows.Forms.Button();
            this.d27 = new System.Windows.Forms.Button();
            this.d26 = new System.Windows.Forms.Button();
            this.d25 = new System.Windows.Forms.Button();
            this.d24 = new System.Windows.Forms.Button();
            this.d23 = new System.Windows.Forms.Button();
            this.d22 = new System.Windows.Forms.Button();
            this.d21 = new System.Windows.Forms.Button();
            this.d20 = new System.Windows.Forms.Button();
            this.d19 = new System.Windows.Forms.Button();
            this.d18 = new System.Windows.Forms.Button();
            this.d17 = new System.Windows.Forms.Button();
            this.d16 = new System.Windows.Forms.Button();
            this.d15 = new System.Windows.Forms.Button();
            this.d14 = new System.Windows.Forms.Button();
            this.d13 = new System.Windows.Forms.Button();
            this.d12 = new System.Windows.Forms.Button();
            this.d11 = new System.Windows.Forms.Button();
            this.d10 = new System.Windows.Forms.Button();
            this.d9 = new System.Windows.Forms.Button();
            this.d8 = new System.Windows.Forms.Button();
            this.d7 = new System.Windows.Forms.Button();
            this.d6 = new System.Windows.Forms.Button();
            this.d5 = new System.Windows.Forms.Button();
            this.d4 = new System.Windows.Forms.Button();
            this.d3 = new System.Windows.Forms.Button();
            this.d2 = new System.Windows.Forms.Button();
            this.d1 = new System.Windows.Forms.Button();
            this.d29 = new System.Windows.Forms.Button();
            this.d31 = new System.Windows.Forms.Button();
            this.d30 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.timeDayofMonth = new DevExpress.XtraEditors.TimeEdit();
            this.rbWeek = new System.Windows.Forms.RadioButton();
            this.rbMonth = new System.Windows.Forms.RadioButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.tb_cntrl.SuspendLayout();
            this.tp_dayofWeek.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeDayofWeek.Properties)).BeginInit();
            this.tp_dayofMonth.SuspendLayout();
            this.tlp_calendar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeDayofMonth.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_cntrl
            // 
            this.tb_cntrl.Controls.Add(this.tp_dayofWeek);
            this.tb_cntrl.Controls.Add(this.tp_dayofMonth);
            this.tb_cntrl.ItemSize = new System.Drawing.Size(0, 1);
            this.tb_cntrl.Location = new System.Drawing.Point(7, 79);
            this.tb_cntrl.Name = "tb_cntrl";
            this.tb_cntrl.SelectedIndex = 0;
            this.tb_cntrl.Size = new System.Drawing.Size(462, 450);
            this.tb_cntrl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tb_cntrl.TabIndex = 1;
            // 
            // tp_dayofWeek
            // 
            this.tp_dayofWeek.BackColor = System.Drawing.Color.White;
            this.tp_dayofWeek.Controls.Add(this.clb_dayofWeek);
            this.tp_dayofWeek.Controls.Add(this.chkEveryday);
            this.tp_dayofWeek.Controls.Add(this.label1);
            this.tp_dayofWeek.Controls.Add(this.timeDayofWeek);
            this.tp_dayofWeek.Location = new System.Drawing.Point(4, 5);
            this.tp_dayofWeek.Name = "tp_dayofWeek";
            this.tp_dayofWeek.Padding = new System.Windows.Forms.Padding(3);
            this.tp_dayofWeek.Size = new System.Drawing.Size(454, 441);
            this.tp_dayofWeek.TabIndex = 1;
            this.tp_dayofWeek.Text = "tabPage2";
            // 
            // clb_dayofWeek
            // 
            this.clb_dayofWeek.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clb_dayofWeek.FormattingEnabled = true;
            this.clb_dayofWeek.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.clb_dayofWeek.Location = new System.Drawing.Point(14, 101);
            this.clb_dayofWeek.Name = "clb_dayofWeek";
            this.clb_dayofWeek.Size = new System.Drawing.Size(256, 180);
            this.clb_dayofWeek.TabIndex = 21;
            // 
            // chkEveryday
            // 
            this.chkEveryday.AutoSize = true;
            this.chkEveryday.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEveryday.Location = new System.Drawing.Point(17, 70);
            this.chkEveryday.Name = "chkEveryday";
            this.chkEveryday.Size = new System.Drawing.Size(87, 21);
            this.chkEveryday.TabIndex = 20;
            this.chkEveryday.Text = "Everyday";
            this.chkEveryday.UseVisualStyleBackColor = true;
            this.chkEveryday.CheckedChanged += new System.EventHandler(this.chkEveryday_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 18);
            this.label1.TabIndex = 19;
            this.label1.Text = "Time:";
            // 
            // timeDayofWeek
            // 
            this.timeDayofWeek.EditValue = new System.DateTime(2020, 5, 11, 0, 0, 0, 0);
            this.timeDayofWeek.Location = new System.Drawing.Point(62, 16);
            this.timeDayofWeek.Name = "timeDayofWeek";
            this.timeDayofWeek.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeDayofWeek.Properties.Appearance.Options.UseFont = true;
            this.timeDayofWeek.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeDayofWeek.Size = new System.Drawing.Size(214, 30);
            this.timeDayofWeek.TabIndex = 18;
            // 
            // tp_dayofMonth
            // 
            this.tp_dayofMonth.BackColor = System.Drawing.Color.White;
            this.tp_dayofMonth.Controls.Add(this.label3);
            this.tp_dayofMonth.Controls.Add(this.chkAllMonth);
            this.tp_dayofMonth.Controls.Add(this.tlp_calendar);
            this.tp_dayofMonth.Controls.Add(this.label2);
            this.tp_dayofMonth.Controls.Add(this.timeDayofMonth);
            this.tp_dayofMonth.Location = new System.Drawing.Point(4, 24);
            this.tp_dayofMonth.Name = "tp_dayofMonth";
            this.tp_dayofMonth.Padding = new System.Windows.Forms.Padding(3);
            this.tp_dayofMonth.Size = new System.Drawing.Size(454, 422);
            this.tp_dayofMonth.TabIndex = 2;
            this.tp_dayofMonth.Text = "tabPage3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 376);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(326, 32);
            this.label3.TabIndex = 25;
            this.label3.Text = "Note: Not all months have the sames number of days, \r\nplease sure to set your sch" +
    "edule accordingly.";
            // 
            // chkAllMonth
            // 
            this.chkAllMonth.AutoSize = true;
            this.chkAllMonth.Enabled = false;
            this.chkAllMonth.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAllMonth.Location = new System.Drawing.Point(17, 70);
            this.chkAllMonth.Name = "chkAllMonth";
            this.chkAllMonth.Size = new System.Drawing.Size(108, 21);
            this.chkAllMonth.TabIndex = 23;
            this.chkAllMonth.Text = "Entire Month";
            this.chkAllMonth.UseVisualStyleBackColor = true;
            this.chkAllMonth.CheckedChanged += new System.EventHandler(this.chkAllMonth_CheckedChanged);
            // 
            // tlp_calendar
            // 
            this.tlp_calendar.ColumnCount = 7;
            this.tlp_calendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp_calendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tlp_calendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tlp_calendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tlp_calendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tlp_calendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tlp_calendar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tlp_calendar.Controls.Add(this.d28, 6, 3);
            this.tlp_calendar.Controls.Add(this.d27, 5, 3);
            this.tlp_calendar.Controls.Add(this.d26, 4, 3);
            this.tlp_calendar.Controls.Add(this.d25, 3, 3);
            this.tlp_calendar.Controls.Add(this.d24, 2, 3);
            this.tlp_calendar.Controls.Add(this.d23, 1, 3);
            this.tlp_calendar.Controls.Add(this.d22, 0, 3);
            this.tlp_calendar.Controls.Add(this.d21, 6, 2);
            this.tlp_calendar.Controls.Add(this.d20, 5, 2);
            this.tlp_calendar.Controls.Add(this.d19, 4, 2);
            this.tlp_calendar.Controls.Add(this.d18, 3, 2);
            this.tlp_calendar.Controls.Add(this.d17, 2, 2);
            this.tlp_calendar.Controls.Add(this.d16, 1, 2);
            this.tlp_calendar.Controls.Add(this.d15, 0, 2);
            this.tlp_calendar.Controls.Add(this.d14, 6, 1);
            this.tlp_calendar.Controls.Add(this.d13, 5, 1);
            this.tlp_calendar.Controls.Add(this.d12, 4, 1);
            this.tlp_calendar.Controls.Add(this.d11, 3, 1);
            this.tlp_calendar.Controls.Add(this.d10, 2, 1);
            this.tlp_calendar.Controls.Add(this.d9, 1, 1);
            this.tlp_calendar.Controls.Add(this.d8, 0, 1);
            this.tlp_calendar.Controls.Add(this.d7, 6, 0);
            this.tlp_calendar.Controls.Add(this.d6, 5, 0);
            this.tlp_calendar.Controls.Add(this.d5, 4, 0);
            this.tlp_calendar.Controls.Add(this.d4, 3, 0);
            this.tlp_calendar.Controls.Add(this.d3, 2, 0);
            this.tlp_calendar.Controls.Add(this.d2, 1, 0);
            this.tlp_calendar.Controls.Add(this.d1, 0, 0);
            this.tlp_calendar.Controls.Add(this.d29, 0, 4);
            this.tlp_calendar.Controls.Add(this.d31, 2, 4);
            this.tlp_calendar.Controls.Add(this.d30, 1, 4);
            this.tlp_calendar.Location = new System.Drawing.Point(14, 97);
            this.tlp_calendar.Name = "tlp_calendar";
            this.tlp_calendar.RowCount = 5;
            this.tlp_calendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp_calendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp_calendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp_calendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp_calendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp_calendar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_calendar.Size = new System.Drawing.Size(425, 269);
            this.tlp_calendar.TabIndex = 24;
            // 
            // d28
            // 
            this.d28.BackColor = System.Drawing.Color.LightGray;
            this.d28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d28.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d28.Location = new System.Drawing.Point(363, 162);
            this.d28.Name = "d28";
            this.d28.Size = new System.Drawing.Size(59, 47);
            this.d28.TabIndex = 27;
            this.d28.Text = "28";
            this.d28.UseVisualStyleBackColor = false;
            this.d28.Click += new System.EventHandler(this.dateClick);
            // 
            // d27
            // 
            this.d27.BackColor = System.Drawing.Color.LightGray;
            this.d27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d27.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d27.Location = new System.Drawing.Point(303, 162);
            this.d27.Name = "d27";
            this.d27.Size = new System.Drawing.Size(54, 47);
            this.d27.TabIndex = 26;
            this.d27.Text = "27";
            this.d27.UseVisualStyleBackColor = false;
            this.d27.Click += new System.EventHandler(this.dateClick);
            // 
            // d26
            // 
            this.d26.BackColor = System.Drawing.Color.LightGray;
            this.d26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d26.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d26.Location = new System.Drawing.Point(243, 162);
            this.d26.Name = "d26";
            this.d26.Size = new System.Drawing.Size(54, 47);
            this.d26.TabIndex = 25;
            this.d26.Text = "26";
            this.d26.UseVisualStyleBackColor = false;
            this.d26.Click += new System.EventHandler(this.dateClick);
            // 
            // d25
            // 
            this.d25.BackColor = System.Drawing.Color.LightGray;
            this.d25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d25.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d25.Location = new System.Drawing.Point(183, 162);
            this.d25.Name = "d25";
            this.d25.Size = new System.Drawing.Size(54, 47);
            this.d25.TabIndex = 24;
            this.d25.Text = "25";
            this.d25.UseVisualStyleBackColor = false;
            this.d25.Click += new System.EventHandler(this.dateClick);
            // 
            // d24
            // 
            this.d24.BackColor = System.Drawing.Color.LightGray;
            this.d24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d24.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d24.Location = new System.Drawing.Point(123, 162);
            this.d24.Name = "d24";
            this.d24.Size = new System.Drawing.Size(54, 47);
            this.d24.TabIndex = 23;
            this.d24.Text = "24";
            this.d24.UseVisualStyleBackColor = false;
            this.d24.Click += new System.EventHandler(this.dateClick);
            // 
            // d23
            // 
            this.d23.BackColor = System.Drawing.Color.LightGray;
            this.d23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d23.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d23.Location = new System.Drawing.Point(63, 162);
            this.d23.Name = "d23";
            this.d23.Size = new System.Drawing.Size(54, 47);
            this.d23.TabIndex = 22;
            this.d23.Text = "23";
            this.d23.UseVisualStyleBackColor = false;
            this.d23.Click += new System.EventHandler(this.dateClick);
            // 
            // d22
            // 
            this.d22.BackColor = System.Drawing.Color.LightGray;
            this.d22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d22.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d22.Location = new System.Drawing.Point(3, 162);
            this.d22.Name = "d22";
            this.d22.Size = new System.Drawing.Size(54, 47);
            this.d22.TabIndex = 21;
            this.d22.Text = "22";
            this.d22.UseVisualStyleBackColor = false;
            this.d22.Click += new System.EventHandler(this.dateClick);
            // 
            // d21
            // 
            this.d21.BackColor = System.Drawing.Color.LightGray;
            this.d21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d21.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d21.Location = new System.Drawing.Point(363, 109);
            this.d21.Name = "d21";
            this.d21.Size = new System.Drawing.Size(59, 47);
            this.d21.TabIndex = 20;
            this.d21.Text = "21";
            this.d21.UseVisualStyleBackColor = false;
            this.d21.Click += new System.EventHandler(this.dateClick);
            // 
            // d20
            // 
            this.d20.BackColor = System.Drawing.Color.LightGray;
            this.d20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d20.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d20.Location = new System.Drawing.Point(303, 109);
            this.d20.Name = "d20";
            this.d20.Size = new System.Drawing.Size(54, 47);
            this.d20.TabIndex = 19;
            this.d20.Text = "20";
            this.d20.UseVisualStyleBackColor = false;
            this.d20.Click += new System.EventHandler(this.dateClick);
            // 
            // d19
            // 
            this.d19.BackColor = System.Drawing.Color.LightGray;
            this.d19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d19.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d19.Location = new System.Drawing.Point(243, 109);
            this.d19.Name = "d19";
            this.d19.Size = new System.Drawing.Size(54, 47);
            this.d19.TabIndex = 18;
            this.d19.Text = "19";
            this.d19.UseVisualStyleBackColor = false;
            this.d19.Click += new System.EventHandler(this.dateClick);
            // 
            // d18
            // 
            this.d18.BackColor = System.Drawing.Color.LightGray;
            this.d18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d18.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d18.Location = new System.Drawing.Point(183, 109);
            this.d18.Name = "d18";
            this.d18.Size = new System.Drawing.Size(54, 47);
            this.d18.TabIndex = 17;
            this.d18.Text = "18";
            this.d18.UseVisualStyleBackColor = false;
            this.d18.Click += new System.EventHandler(this.dateClick);
            // 
            // d17
            // 
            this.d17.BackColor = System.Drawing.Color.LightGray;
            this.d17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d17.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d17.Location = new System.Drawing.Point(123, 109);
            this.d17.Name = "d17";
            this.d17.Size = new System.Drawing.Size(54, 47);
            this.d17.TabIndex = 16;
            this.d17.Text = "17";
            this.d17.UseVisualStyleBackColor = false;
            this.d17.Click += new System.EventHandler(this.dateClick);
            // 
            // d16
            // 
            this.d16.BackColor = System.Drawing.Color.LightGray;
            this.d16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d16.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d16.Location = new System.Drawing.Point(63, 109);
            this.d16.Name = "d16";
            this.d16.Size = new System.Drawing.Size(54, 47);
            this.d16.TabIndex = 15;
            this.d16.Text = "16";
            this.d16.UseVisualStyleBackColor = false;
            this.d16.Click += new System.EventHandler(this.dateClick);
            // 
            // d15
            // 
            this.d15.BackColor = System.Drawing.Color.LightGray;
            this.d15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d15.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d15.Location = new System.Drawing.Point(3, 109);
            this.d15.Name = "d15";
            this.d15.Size = new System.Drawing.Size(54, 47);
            this.d15.TabIndex = 14;
            this.d15.Text = "15";
            this.d15.UseVisualStyleBackColor = false;
            this.d15.Click += new System.EventHandler(this.dateClick);
            // 
            // d14
            // 
            this.d14.BackColor = System.Drawing.Color.LightGray;
            this.d14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d14.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d14.Location = new System.Drawing.Point(363, 56);
            this.d14.Name = "d14";
            this.d14.Size = new System.Drawing.Size(59, 47);
            this.d14.TabIndex = 13;
            this.d14.Text = "14";
            this.d14.UseVisualStyleBackColor = false;
            this.d14.Click += new System.EventHandler(this.dateClick);
            // 
            // d13
            // 
            this.d13.BackColor = System.Drawing.Color.LightGray;
            this.d13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d13.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d13.Location = new System.Drawing.Point(303, 56);
            this.d13.Name = "d13";
            this.d13.Size = new System.Drawing.Size(54, 47);
            this.d13.TabIndex = 12;
            this.d13.Text = "13";
            this.d13.UseVisualStyleBackColor = false;
            this.d13.Click += new System.EventHandler(this.dateClick);
            // 
            // d12
            // 
            this.d12.BackColor = System.Drawing.Color.LightGray;
            this.d12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d12.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d12.Location = new System.Drawing.Point(243, 56);
            this.d12.Name = "d12";
            this.d12.Size = new System.Drawing.Size(54, 47);
            this.d12.TabIndex = 11;
            this.d12.Text = "12";
            this.d12.UseVisualStyleBackColor = false;
            this.d12.Click += new System.EventHandler(this.dateClick);
            // 
            // d11
            // 
            this.d11.BackColor = System.Drawing.Color.LightGray;
            this.d11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d11.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d11.Location = new System.Drawing.Point(183, 56);
            this.d11.Name = "d11";
            this.d11.Size = new System.Drawing.Size(54, 47);
            this.d11.TabIndex = 10;
            this.d11.Text = "11";
            this.d11.UseVisualStyleBackColor = false;
            this.d11.Click += new System.EventHandler(this.dateClick);
            // 
            // d10
            // 
            this.d10.BackColor = System.Drawing.Color.LightGray;
            this.d10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d10.Location = new System.Drawing.Point(123, 56);
            this.d10.Name = "d10";
            this.d10.Size = new System.Drawing.Size(54, 47);
            this.d10.TabIndex = 9;
            this.d10.Text = "10";
            this.d10.UseVisualStyleBackColor = false;
            this.d10.Click += new System.EventHandler(this.dateClick);
            // 
            // d9
            // 
            this.d9.BackColor = System.Drawing.Color.LightGray;
            this.d9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d9.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d9.Location = new System.Drawing.Point(63, 56);
            this.d9.Name = "d9";
            this.d9.Size = new System.Drawing.Size(54, 47);
            this.d9.TabIndex = 8;
            this.d9.Text = "9";
            this.d9.UseVisualStyleBackColor = false;
            this.d9.Click += new System.EventHandler(this.dateClick);
            // 
            // d8
            // 
            this.d8.BackColor = System.Drawing.Color.LightGray;
            this.d8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d8.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d8.Location = new System.Drawing.Point(3, 56);
            this.d8.Name = "d8";
            this.d8.Size = new System.Drawing.Size(54, 47);
            this.d8.TabIndex = 7;
            this.d8.Text = "8";
            this.d8.UseVisualStyleBackColor = false;
            this.d8.Click += new System.EventHandler(this.dateClick);
            // 
            // d7
            // 
            this.d7.BackColor = System.Drawing.Color.LightGray;
            this.d7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d7.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d7.Location = new System.Drawing.Point(363, 3);
            this.d7.Name = "d7";
            this.d7.Size = new System.Drawing.Size(59, 47);
            this.d7.TabIndex = 6;
            this.d7.Text = "7";
            this.d7.UseVisualStyleBackColor = false;
            this.d7.Click += new System.EventHandler(this.dateClick);
            // 
            // d6
            // 
            this.d6.BackColor = System.Drawing.Color.LightGray;
            this.d6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d6.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d6.Location = new System.Drawing.Point(303, 3);
            this.d6.Name = "d6";
            this.d6.Size = new System.Drawing.Size(54, 47);
            this.d6.TabIndex = 5;
            this.d6.Text = "6";
            this.d6.UseVisualStyleBackColor = false;
            this.d6.Click += new System.EventHandler(this.dateClick);
            // 
            // d5
            // 
            this.d5.BackColor = System.Drawing.Color.LightGray;
            this.d5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d5.Location = new System.Drawing.Point(243, 3);
            this.d5.Name = "d5";
            this.d5.Size = new System.Drawing.Size(54, 47);
            this.d5.TabIndex = 4;
            this.d5.Text = "5";
            this.d5.UseVisualStyleBackColor = false;
            this.d5.Click += new System.EventHandler(this.dateClick);
            // 
            // d4
            // 
            this.d4.BackColor = System.Drawing.Color.LightGray;
            this.d4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d4.Location = new System.Drawing.Point(183, 3);
            this.d4.Name = "d4";
            this.d4.Size = new System.Drawing.Size(54, 47);
            this.d4.TabIndex = 3;
            this.d4.Text = "4";
            this.d4.UseVisualStyleBackColor = false;
            this.d4.Click += new System.EventHandler(this.dateClick);
            // 
            // d3
            // 
            this.d3.BackColor = System.Drawing.Color.LightGray;
            this.d3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d3.Location = new System.Drawing.Point(123, 3);
            this.d3.Name = "d3";
            this.d3.Size = new System.Drawing.Size(54, 47);
            this.d3.TabIndex = 2;
            this.d3.Text = "3";
            this.d3.UseVisualStyleBackColor = false;
            this.d3.Click += new System.EventHandler(this.dateClick);
            // 
            // d2
            // 
            this.d2.BackColor = System.Drawing.Color.LightGray;
            this.d2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d2.Location = new System.Drawing.Point(63, 3);
            this.d2.Name = "d2";
            this.d2.Size = new System.Drawing.Size(54, 47);
            this.d2.TabIndex = 1;
            this.d2.Text = "2";
            this.d2.UseVisualStyleBackColor = false;
            this.d2.Click += new System.EventHandler(this.dateClick);
            // 
            // d1
            // 
            this.d1.BackColor = System.Drawing.Color.LightGray;
            this.d1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d1.Location = new System.Drawing.Point(3, 3);
            this.d1.Name = "d1";
            this.d1.Size = new System.Drawing.Size(54, 47);
            this.d1.TabIndex = 0;
            this.d1.Text = "1";
            this.d1.UseVisualStyleBackColor = false;
            this.d1.Click += new System.EventHandler(this.dateClick);
            // 
            // d29
            // 
            this.d29.BackColor = System.Drawing.Color.LightGray;
            this.d29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d29.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d29.Location = new System.Drawing.Point(3, 215);
            this.d29.Name = "d29";
            this.d29.Size = new System.Drawing.Size(54, 51);
            this.d29.TabIndex = 28;
            this.d29.Text = "29";
            this.d29.UseVisualStyleBackColor = false;
            this.d29.Click += new System.EventHandler(this.dateClick);
            // 
            // d31
            // 
            this.d31.BackColor = System.Drawing.Color.LightGray;
            this.d31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d31.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d31.Location = new System.Drawing.Point(123, 215);
            this.d31.Name = "d31";
            this.d31.Size = new System.Drawing.Size(54, 51);
            this.d31.TabIndex = 30;
            this.d31.Text = "31";
            this.d31.UseVisualStyleBackColor = false;
            this.d31.Click += new System.EventHandler(this.dateClick);
            // 
            // d30
            // 
            this.d30.BackColor = System.Drawing.Color.LightGray;
            this.d30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.d30.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d30.Location = new System.Drawing.Point(63, 215);
            this.d30.Name = "d30";
            this.d30.Size = new System.Drawing.Size(54, 51);
            this.d30.TabIndex = 29;
            this.d30.Text = "30";
            this.d30.UseVisualStyleBackColor = false;
            this.d30.Click += new System.EventHandler(this.dateClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 21;
            this.label2.Text = "Time:";
            // 
            // timeDayofMonth
            // 
            this.timeDayofMonth.EditValue = new System.DateTime(2020, 5, 11, 0, 0, 0, 0);
            this.timeDayofMonth.Location = new System.Drawing.Point(62, 16);
            this.timeDayofMonth.Name = "timeDayofMonth";
            this.timeDayofMonth.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeDayofMonth.Properties.Appearance.Options.UseFont = true;
            this.timeDayofMonth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeDayofMonth.Size = new System.Drawing.Size(214, 30);
            this.timeDayofMonth.TabIndex = 20;
            // 
            // rbWeek
            // 
            this.rbWeek.AutoSize = true;
            this.rbWeek.BackColor = System.Drawing.Color.White;
            this.rbWeek.Checked = true;
            this.rbWeek.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbWeek.Location = new System.Drawing.Point(17, 34);
            this.rbWeek.Name = "rbWeek";
            this.rbWeek.Size = new System.Drawing.Size(129, 24);
            this.rbWeek.TabIndex = 3;
            this.rbWeek.TabStop = true;
            this.rbWeek.Text = "Day of Week";
            this.rbWeek.UseVisualStyleBackColor = false;
            this.rbWeek.CheckedChanged += new System.EventHandler(this.rbWeek_CheckedChanged);
            // 
            // rbMonth
            // 
            this.rbMonth.AutoSize = true;
            this.rbMonth.BackColor = System.Drawing.Color.White;
            this.rbMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMonth.Location = new System.Drawing.Point(195, 34);
            this.rbMonth.Name = "rbMonth";
            this.rbMonth.Size = new System.Drawing.Size(134, 24);
            this.rbMonth.TabIndex = 4;
            this.rbMonth.Text = "Day of Month";
            this.rbMonth.UseVisualStyleBackColor = false;
            this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(271, 550);
            this.btnSave.LookAndFeel.SkinMaskColor = System.Drawing.Color.SeaGreen;
            this.btnSave.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.btnSave.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(193, 57);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ctrlBackupSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.rbMonth);
            this.Controls.Add(this.rbWeek);
            this.Controls.Add(this.tb_cntrl);
            this.Name = "ctrlBackupSchedule";
            this.Size = new System.Drawing.Size(477, 636);
            this.tb_cntrl.ResumeLayout(false);
            this.tp_dayofWeek.ResumeLayout(false);
            this.tp_dayofWeek.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeDayofWeek.Properties)).EndInit();
            this.tp_dayofMonth.ResumeLayout(false);
            this.tp_dayofMonth.PerformLayout();
            this.tlp_calendar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.timeDayofMonth.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TabControl tb_cntrl;
        public System.Windows.Forms.TabPage tp_dayofWeek;
        public System.Windows.Forms.TabPage tp_dayofMonth;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TimeEdit timeDayofWeek;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TimeEdit timeDayofMonth;
        private System.Windows.Forms.RadioButton rbWeek;
        private System.Windows.Forms.RadioButton rbMonth;
        private System.Windows.Forms.CheckedListBox clb_dayofWeek;
        private System.Windows.Forms.CheckBox chkEveryday;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.CheckBox chkAllMonth;
        private System.Windows.Forms.TableLayoutPanel tlp_calendar;
        private System.Windows.Forms.Button d30;
        private System.Windows.Forms.Button d28;
        private System.Windows.Forms.Button d27;
        private System.Windows.Forms.Button d26;
        private System.Windows.Forms.Button d25;
        private System.Windows.Forms.Button d24;
        private System.Windows.Forms.Button d23;
        private System.Windows.Forms.Button d22;
        private System.Windows.Forms.Button d21;
        private System.Windows.Forms.Button d20;
        private System.Windows.Forms.Button d19;
        private System.Windows.Forms.Button d18;
        private System.Windows.Forms.Button d17;
        private System.Windows.Forms.Button d16;
        private System.Windows.Forms.Button d15;
        private System.Windows.Forms.Button d14;
        private System.Windows.Forms.Button d13;
        private System.Windows.Forms.Button d12;
        private System.Windows.Forms.Button d11;
        private System.Windows.Forms.Button d10;
        private System.Windows.Forms.Button d9;
        private System.Windows.Forms.Button d8;
        private System.Windows.Forms.Button d7;
        private System.Windows.Forms.Button d6;
        private System.Windows.Forms.Button d5;
        private System.Windows.Forms.Button d4;
        private System.Windows.Forms.Button d3;
        private System.Windows.Forms.Button d2;
        private System.Windows.Forms.Button d1;
        private System.Windows.Forms.Button d29;
        private System.Windows.Forms.Button d31;
        private DevExpress.XtraEditors.SimpleButton btnSave;
    }
}
