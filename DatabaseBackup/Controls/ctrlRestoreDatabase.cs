﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using posBang.Library.Helper;

namespace DatabaseBackup
{
    public partial class ctrlRestoreDatabase : UserControl
    {

        public string Host { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string SavePath { get; set; }
        public string ConnectionString { get; set; }

        public string Transaction { get; set; }
      public string ImportPercentage { get; set; }


        public ctrlRestoreDatabase()
        {
            InitializeComponent();
        }


        #region Functions


        public void Connect()
        {
            try
            {
                SettingFile settings = new SettingFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");

               settings.OpenSection("credentials");
               Host = settings.Read<string>("Host", "");
               User = settings.Read<string>("User", "");
               Password = settings.ReadProtect<string>("Password", "");
               Port = settings.Read<string>("Port", "");
               Database = settings.Read<string>("Database","");
               settings.CloseSection();

               settings.OpenSection("schedule");
               SavePath = settings.Read<string>("SavePath", "");

               settings.CloseSection();
            
                ConnectionString = "server = " + Host + ";user= " + User + ";pwd=" + Password + ";port=" + Port + ";database=" + Database + ";";
            }
            catch (Exception ex)
            { MessageBox.Show("Failed in connecting to server."); }
        }

        public void BackupDatabase()
        {
            
            ConnectionString += "charset=utf8;convertzerodatetime=true;";

            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    using (MySqlBackup mb = new MySqlBackup(cmd))
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        mb.ExportInfo.AddCreateDatabase = true;
                       
                        mb.ExportInfo.AddDropTable = true;
                        mb.ExportInfo.ExportEvents = true;
                        mb.ExportInfo.ExportFunctions = true;
                        mb.ExportInfo.ExportProcedures = true;
                        mb.ExportInfo.ExportRoutinesWithoutDefiner = true;
                        mb.ExportInfo.ExportRows = true;
                        mb.ExportInfo.ExportTableStructure = true;
                        mb.ExportInfo.ExportTriggers = true;
                        mb.ExportInfo.ExportViews = true;
                        mb.ExportProgressChanged += Mb_ExportProgressChanged;
                        mb.ExportToFile(txtRestoreDatabase.Text);

                       
                        conn.Close();
                    }
                }
            }
            progressBar1.PerformStep();
            progressBar1.PerformStep();
            progressBar1.PerformStep();
            progressBar1.PerformStep();
            txtStatus.Text = "Backup Completed";
        
        }

        public void RestoreDatabase()
        {
            ConnectionString = "server = " + Host + ";user= " + User + ";pwd=" + Password + ";port=" + Port + ";";
          

            using (MySqlConnection conn = new MySqlConnection(ConnectionString + "charset=utf8;convertzerodatetime=true;"))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    using (MySqlBackup mb = new MySqlBackup(cmd))
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        mb.ImportProgressChanged += Mb_ImportProgressChanged;
                        mb.ImportFromFile(txtRestoreDatabase.Text);

                        conn.Close();
                    }
                }
            }
            progressBar1.PerformStep();
            progressBar1.PerformStep();
            progressBar1.PerformStep();
            progressBar1.PerformStep();
            txtStatus.Text = "Restore Completed";

        }

        private void Mb_ImportProgressChanged(object sender, ImportProgressArgs e)
        {
            ImportPercentage = e.PercentageCompleted.ToString();
            SetText(ImportPercentage);
           // progressBar1.PerformStep();
            // throw new NotImplementedException();
        }

        private void Mb_ExportProgressChanged(object sender, ExportProgressArgs e)
        {
                             
            txtStatus.Text = "Exporting table: " + e.CurrentTableName;
            progressBar1.PerformStep();

            //throw new NotImplementedException();
        }

        delegate void SetTextCallback(string text);
        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.txtStatus.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.txtStatus.Text = text;
            }
        }


        #endregion

        private void ctrlBackupAndRestore_Load(object sender, EventArgs e)
        {

        }

        private void buttonEdit1_Enter(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtRestoreDatabase.Text = openFileDialog1.FileName;
            }
        }

        private void txtRestoreDatabase_Enter(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtRestoreDatabase.Text = openFileDialog1.FileName;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
           
            progressBar1.Value = 0;
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Connect();      
            RestoreDatabase();    
        }

        private void txtStatus_Click(object sender, EventArgs e)
        {

        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            Transaction = "Restore";
            progressBar1.Value = 0;
            backgroundWorker1.RunWorkerAsync();
        }

        private void txtStatus_TextChanged(object sender, EventArgs e)
        {
            progressBar1.PerformStep();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
    }
}
