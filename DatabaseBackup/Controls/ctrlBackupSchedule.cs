﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using posBang.Library.Helper;
using System.IO;

namespace DatabaseBackup
{
    public partial class ctrlBackupSchedule : UserControl
    {

        public List<string> dateSelected = new List<string>();
        public List<string> daysSelected = new List<string>();
        public string ScheduleType = "";


        public ctrlBackupSchedule()
        {
            InitializeComponent();
            if(rbMonth.Checked)
            {
                ScheduleType = "Day of Month";
            }
            else
            {
                ScheduleType = "Day of Week";
            }

            LoadSettings();
        }


       

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void rbWeek_CheckedChanged(object sender, EventArgs e)
        {
            if(rbWeek.Checked)
            {
                tb_cntrl.SelectedTab = tp_dayofWeek;
                ScheduleType = "Day of Week";
            }
        }

        private void rbMonth_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMonth.Checked)
            {
                tb_cntrl.SelectedTab = tp_dayofMonth;
                ScheduleType = "Day of Month";
            }
        }

        private void chkEveryday_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEveryday.Checked)
            {
                for (int x = 0; x <= clb_dayofWeek.Items.Count - 1; x++)
                {
                    clb_dayofWeek.SetItemChecked(x, true);
                }
            }
            else
            {
                for (int x = 0; x <= clb_dayofWeek.Items.Count - 1; x++)
                {
                    clb_dayofWeek.SetItemChecked(x, false);
                }
            }
        }

        private void chkAllMonth_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAllMonth.Checked == true)
            {
                foreach (Control ctrl in tlp_calendar.Controls)

                    if (ctrl is Button)
                    {
                        (ctrl as Button).BackColor = Color.SteelBlue;                  
                        dateSelected.Add(ctrl.Text); 
                    }
            }
            else
            {
                foreach (Control ctrl in tlp_calendar.Controls)

                    if (ctrl is Button)
                    {
                        (ctrl as Button).BackColor = Color.LightGray;
                        dateSelected.Remove(ctrl.Text);
                    }
            }
        }

        public void dateClick(object sender, EventArgs e)
        {
            string date = (sender as Button).Text;
            if ((sender as Button).BackColor == Color.LightGray)
            {
                (sender as Button).BackColor = Color.SteelBlue;
                dateSelected.Add(date);
            }
            else
            {
                (sender as Button).BackColor = Color.LightGray;
                dateSelected.Remove(date);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            daysSelected.Clear();
            foreach (int item in clb_dayofWeek.CheckedIndices)
            {
                clb_dayofWeek.Items[item].ToString();
                daysSelected.Add(clb_dayofWeek.Items[item].ToString());
            }

            
            String days = String.Join(",", daysSelected);
            String dates = String.Join(",", dateSelected);

            SettingFile settings = new SettingFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");
            settings.OpenSection("schedule");
            settings.Write("ScheduleType", ScheduleType);
            settings.Write("DayOfWeek", days);
            settings.Write("DayOfMonth", dates);

            if(ScheduleType == "Day of Week")
            {
                settings.Write("Time", timeDayofWeek.Text);
            }
            else
            {
                settings.Write("Time", timeDayofMonth.Text);
            }
            settings.CloseSection();

            MessageBox.Show("Configuration Saved!");

        }

        public void LoadSettings()
        {
            try
            {
                if(File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini"))
                {
                    SettingFile settings = new SettingFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");
                    settings.OpenSection("schedule");
                    string scheduleType = settings.Read<string>("ScheduleType", ScheduleType);
                    string daysOfWeek = settings.Read<string>("DayOfWeek", "");
                    string daysOfMonth = settings.Read<string>("DayOfMonth", "");
                    string time = settings.Read<string>("Time", "");


                    if(scheduleType == "Day of Month")
                    {
                        rbMonth.Checked = true;
                        rbWeek.Checked = false;
                        tb_cntrl.SelectedTab = tp_dayofMonth;
                        timeDayofMonth.Time = time.ToDateTime();
                        List<string> result = daysOfMonth.Split(',').ToList();

                        foreach(string item in result)
                        {
                            foreach(Control c in tlp_calendar.Controls)
                            {
                                if (c.Text == item)
                                {
                                    string date = (c as Button).Text;
                                    if ((c as Button).BackColor == Color.LightGray)
                                    {
                                        (c as Button).BackColor = Color.SteelBlue;
                                        dateSelected.Add(date);
                                    }
                                }                             
                            }
                        }
                    }
                    else
                    {
                        rbMonth.Checked = false;
                        rbWeek.Checked = true;
                        tb_cntrl.SelectedTab = tp_dayofWeek;
                        timeDayofWeek.Time = time.ToDateTime();
                        List<string> result = daysOfWeek.Split(',').ToList();

                        foreach (string item in result)
                        {
                            for (int x = 0; x <= clb_dayofWeek.Items.Count - 1; x++)
                            {
                                string s = clb_dayofWeek.Items[x].ToString();
                                if (clb_dayofWeek.Items[x].ToString() == item)
                                {
                                    clb_dayofWeek.SetItemChecked(x, true);
                                }
                              
                            }
                        }
                    }

                }
               
            }
            catch { }
        }
    }
}
