﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceProcess;
using System.IO;

namespace DatabaseBackup
{
    public partial class frmMain : Form
    {

        string Service_Status { get; set; }
        public frmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();
            ctrlHome ctrl = new ctrlHome();
            pnlMain.Controls.Add(ctrl);
            ServiceStatus();

            //if(File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini"))
            //{
            //    this.WindowState = FormWindowState.Minimized;
            //    this.ShowInTaskbar = false;
            //}


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();

            ctrlConfiguration ctrl = new ctrlConfiguration();
            pnlMain.Controls.Add(ctrl);
        }

        private void btnBackupAndRestore_Click(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();
            ctrlBackupAndRestore ctrl = new ctrlBackupAndRestore();
            pnlMain.Controls.Add(ctrl);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmExit_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Are you sure you want to exit.", "Exit", MessageBoxButtons.YesNo ) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }



        public void ServiceStatus()
        {
            try
            {
                ServiceController sc = new ServiceController("DatabaseBackup");

                if (sc.Status.ToString() == Service_Status)
                {

                }
                else
                {
                    switch (sc.Status)
                    {
                        case ServiceControllerStatus.Running:
                            notifyIcon1.ShowBalloonTip(1, "Status", "Database Backup is Running.", ToolTipIcon.Info);
                            notifyIcon1.Icon = Properties.Resources.service_running;
                            cmStatus.Text = "Running";
                            cmCommand.Text = "Stop";
                            Service_Status = "Running";
                            tsServiceControl.IsOn = true;
                            lblServicecontrol.Text = "Service Running";
                            break;
                        case ServiceControllerStatus.Stopped:
                            notifyIcon1.ShowBalloonTip(1, "Status", "Database Backup has Stopped.", ToolTipIcon.Error);
                            notifyIcon1.Icon = Properties.Resources.service_stopped;
                            cmStatus.Text = "Stopped";
                            cmCommand.Text = "Start";
                            Service_Status = "Stopped";
                            tsServiceControl.IsOn = false;
                            lblServicecontrol.Text = "Service Stopped";
                            break;
                        case ServiceControllerStatus.Paused:
                            notifyIcon1.ShowBalloonTip(1, "Status", "Database Backup has Paused.", ToolTipIcon.Info);
                            Service_Status = "Paused";
                            tsServiceControl.IsOn = false;
                            lblServicecontrol.Text = "Service Paused";
                            break;
                    }
                }
            }
            catch { }
        }

        private void cmOpenConfig_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void cmCommand_Click(object sender, EventArgs e)
        {
            if(cmCommand.Text == "Start")
            {
                ServiceController service = new ServiceController("DatabaseBackup");

                if ((service.Status.Equals(ServiceControllerStatus.Stopped)))
                {
                    service.Start();
                }         
                cmCommand.Text = "Stop";
            }
            else
            {
                ServiceController service = new ServiceController("DatabaseBackup");

                if ((service.Status.Equals(ServiceControllerStatus.Running)))
                {
                    service.Stop();
                }

                cmCommand.Text = "Start";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            ServiceStatus();
            timer1.Start();
        }

        private void pnlBackupDatabase_Click(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();
            ctrlBackupAndRestore ctrl = new ctrlBackupAndRestore();
            pnlMain.Controls.Add(ctrl);

            #region
            pnlHome.BorderStyle = BorderStyle.None;
            pnlConnectionSetup.BorderStyle = BorderStyle.None;
            pnlBackupSchedule.BorderStyle = BorderStyle.None;
            pnlBackupDatabase.BorderStyle = BorderStyle.FixedSingle;
            pnlRestoreDatabase.BorderStyle = BorderStyle.None;
            pnlMinimize.BorderStyle = BorderStyle.None;
            #endregion
        }

        private void pnlRestoreDatabase_Click(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();
            ctrlRestoreDatabase ctrl = new ctrlRestoreDatabase();
            pnlMain.Controls.Add(ctrl);

            #region
            pnlHome.BorderStyle = BorderStyle.None;
            pnlConnectionSetup.BorderStyle = BorderStyle.None;
            pnlBackupSchedule.BorderStyle = BorderStyle.None;
            pnlBackupDatabase.BorderStyle = BorderStyle.None;
            pnlRestoreDatabase.BorderStyle = BorderStyle.FixedSingle;
            pnlMinimize.BorderStyle = BorderStyle.None;
            #endregion
        }

        private void pnlConnectionSetup_Click(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();
            ctrlConfiguration ctrl = new ctrlConfiguration();
            pnlMain.Controls.Add(ctrl);
            #region
            pnlHome.BorderStyle = BorderStyle.None;
            pnlConnectionSetup.BorderStyle = BorderStyle.FixedSingle;
            pnlBackupSchedule.BorderStyle = BorderStyle.None;
            pnlBackupDatabase.BorderStyle = BorderStyle.None;
            pnlRestoreDatabase.BorderStyle = BorderStyle.None;
            pnlMinimize.BorderStyle = BorderStyle.None;
            #endregion
        }

        private void pnlBackupSchedule_Click(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();
            ctrlBackupSchedule ctrl = new ctrlBackupSchedule();
            pnlMain.Controls.Add(ctrl);
            #region
            pnlHome.BorderStyle = BorderStyle.None;
            pnlConnectionSetup.BorderStyle = BorderStyle.None;
            pnlBackupSchedule.BorderStyle = BorderStyle.FixedSingle;
            pnlBackupDatabase.BorderStyle = BorderStyle.None;
            pnlRestoreDatabase.BorderStyle = BorderStyle.None;
            pnlMinimize.BorderStyle = BorderStyle.None;
            #endregion

        }

        private void pnlHome_Click(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();
            ctrlHome ctrl = new ctrlHome();
            pnlMain.Controls.Add(ctrl);


            #region
            pnlHome.BorderStyle = BorderStyle.FixedSingle;
            pnlConnectionSetup.BorderStyle = BorderStyle.None;
            pnlBackupSchedule.BorderStyle = BorderStyle.None;
            pnlBackupDatabase.BorderStyle = BorderStyle.None;
            pnlRestoreDatabase.BorderStyle = BorderStyle.None;
            pnlMinimize.BorderStyle = BorderStyle.None;         
            #endregion
        }

        private void tsServiceControl_Toggled(object sender, EventArgs e)
        {
            if (tsServiceControl.IsOn == true)
            {
                ServiceController service = new ServiceController("DatabaseBackup");

                if ((service.Status.Equals(ServiceControllerStatus.Stopped)))
                {
                    service.Start();
                }
               
            }
            else
            {
                ServiceController service = new ServiceController("DatabaseBackup");

                if ((service.Status.Equals(ServiceControllerStatus.Running)))
                {
                    service.Stop();
                }

            }
        }

        private void pnlDestination_Click(object sender, EventArgs e)
        {
            pnlMain.Controls.Clear();
            ctrlSavePath ctrl = new ctrlSavePath();
            pnlMain.Controls.Add(ctrl);
            #region
            pnlDestination.BorderStyle = BorderStyle.FixedSingle;
            pnlHome.BorderStyle = BorderStyle.None;
            pnlConnectionSetup.BorderStyle = BorderStyle.None;
            pnlBackupSchedule.BorderStyle = BorderStyle.None;
            pnlBackupDatabase.BorderStyle = BorderStyle.None;
            pnlRestoreDatabase.BorderStyle = BorderStyle.None;
            pnlMinimize.BorderStyle = BorderStyle.None;
            #endregion
        }
    }
}
