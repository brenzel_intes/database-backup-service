﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using posBang.Library.Helper;
using MySql.Data.MySqlClient;

using System.IO;

using System.Timers;

namespace DatabaseBackupService
{
    public partial class Service1 : ServiceBase
    {

        public string Host { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string SavePath { get; set; }
        public string ConnectionString { get; set; }

        public string IntervalType { get; set; }

        public string Interval { get; set; }


        string scheduleType = "";
        List<string> scheduledBackupsDates = new List<string>();
        List<string> scheduledBackupsDays = new List<string>();
        public DateTime scheduledBackupTime = new DateTime();

        Timer timer = new Timer();

        public Service1()
        {

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {     
            WriteToFile("Service Start");
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            InitializeSettings();
            timer.Interval = 1000;  
            timer.Enabled = true;

            //String applicationName = AppDomain.CurrentDomain.BaseDirectory + @"DatabaseBackup.exe";         
            //System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("DatabaseBackup");
            //// Before starting the new process make sure no other MyProcessName is running.
            //foreach (System.Diagnostics.Process p in process)
            //{
            //    p.Kill();
            //}

            //// launch the application
            //ApplicationLoader.PROCESS_INFORMATION procInfo;
            //ApplicationLoader.StartProcessAndBypassUAC(applicationName, out procInfo);
            

        }


        private enum SimpleServiceCustomCommands { KillProcess = 128 };

        protected override void OnCustomCommand(int command)
        {
            switch (command)
            {
                case (int)SimpleServiceCustomCommands.KillProcess:
                   
                        System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("MyProcessName");
                        // Before starting the new process make sure no other MyProcessName is running.
                        foreach (System.Diagnostics.Process p in process)
                        {
                            p.Kill();
                        }
                    
                    break;
                default:
                    break;
            }
        }


        private void OnElapsedTime(object sender, ElapsedEventArgs e)
        {
            try
            {
               
                if (scheduledBackupsDays.Contains(DateTime.Now.DayOfWeek.ToString()) == true)
                {
                   
                    if (DateTime.Now.ToString("hh:mm:ss tt") == scheduledBackupTime.ToString("hh:mm:ss tt"))
                    {
                        BackupDatabase();
                       
                    }
                }


                if (scheduledBackupsDates.Contains(DateTime.Now.Date.Day.ToString()) == true)
                {
                  
                    if (DateTime.Now.ToString("hh:mm:ss tt") == scheduledBackupTime.ToString("hh:mm:ss tt"))
                    {
                        BackupDatabase();
                       
                    }
                }

            }
            catch { }                                                     
        }

        protected override void OnStop()
        {
            WriteToFile("Service Stopped");
           // timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
           // InitializeSettings();
           // timer.Enabled = true;

            //String applicationName = AppDomain.CurrentDomain.BaseDirectory + @"DatabaseBackup.exe";
            //System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("DatabaseBackup");
            //// Before starting the new process make sure no other MyProcessName is running.
            //foreach (System.Diagnostics.Process p in process)
            //{
            //    p.Kill();
            //}

            //// launch the application
            //ApplicationLoader.PROCESS_INFORMATION procInfo;
            //ApplicationLoader.StartProcessAndBypassUAC(applicationName, out procInfo);

        }

        #region Functions

        public void InitializeSettings()
        {
            try
            {
                string settings_file = AppDomain.CurrentDomain.BaseDirectory + @"settings.ini";
                string dbName = "";
                
                if(!File.Exists(settings_file))
                {
                    string con = "server=localhost;user=admin;pwd=21posincbibiza4012006;port=2121";
                    using (MySqlConnection conn = new MySqlConnection(con))
                    {
                        conn.Open();
                        DataTable dt = new DataTable();
                        MySqlCommand cmd = new MySqlCommand("show databases", conn);
                        cmd.ExecuteNonQuery();
                        MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                        da.Fill(dt);

                        foreach (DataRow row in dt.Rows)
                        {
                            if (row[0].ToString() == "krypton")
                            {
                                dbName = "krypton";
                            }
                        }

                    }


                    SettingFile setting = new SettingFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");
                    setting.OpenSection("credentials");
                    setting.Write("Host", "localhost");
                    setting.Write("User", "admin");
                    setting.WriteProtect("Password", "21posincbibiza4012006");
                    setting.Write("Port", "2121");
                    setting.Write("Database", dbName == "krypton" ? "krypton" : "agiladb");
                    setting.CloseSection();

                    setting.OpenSection("output");
                    setting.Write("SavePath", AppDomain.CurrentDomain.BaseDirectory + @"Backups");
                    setting.CloseSection();

                    setting.OpenSection("schedule");
                    setting.Write("ScheduleType", "Day of Week");
                    setting.Write("DayOfWeek", "Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday");
                    setting.Write("DayOfMonth", "");
                    setting.Write("Time", "4:00:00 PM");
                    setting.CloseSection();


                } 
                

                SettingFile settings = new SettingFile(AppDomain.CurrentDomain.BaseDirectory + @"\settings.ini");
                settings.OpenSection("credentials");
                Host = settings.Read<string>("Host", "");
                User = settings.Read<string>("User", "");
                Password = settings.ReadProtect<string>("Password", "");
                Port = settings.Read<string>("Port", "");
                Database = settings.Read<string>("Database", "");
                settings.CloseSection();

                settings.OpenSection("output");
                SavePath = settings.Read<string>("SavePath", "");
                settings.CloseSection();

                settings.OpenSection("schedule");
                scheduleType = settings.Read<string>("ScheduleType", "");
                string dayOfWeek = settings.Read<string>("DayOfWeek", "");
                string dayOfMonth = settings.Read<string>("DayOfMonth", "");
                string time = settings.Read<string>("Time", "");
                settings.CloseSection();

                if (scheduleType == "Day of Week")
                {
                    scheduledBackupsDays = dayOfWeek.Split(',').ToList<string>();
                    scheduledBackupsDates.Clear();
                    WriteToFile("Backup scheduled every " + dayOfWeek + " at " + time);
                }
                else if (scheduleType == "Day of Month")
                {
                    scheduledBackupsDates = dayOfMonth.Split(',').ToList<string>();
                    scheduledBackupsDays.Clear();
                    WriteToFile("Backup scheduled every " + dayOfMonth + " at " + time);
                }
            
                DateTime.TryParse(time, out scheduledBackupTime);                                      
               ConnectionString = "server = " + Host + ";user= " + User + ";pwd=" + Password + ";port=" + Port + ";database=" + Database + ";";

            }
            catch (Exception ex)
            {
                WriteToFile(ex.ToString());
            }
        }


        public void BackupDatabase()
        {
            try
            {           
                string file = SavePath + @"\" + DateTime.Now.Date.ToString("MM-dd-yyyy") + ".sql";
                using (MySqlConnection conn = new MySqlConnection(ConnectionString + "charset=utf8;convertzerodatetime=true;"))
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        using (MySqlBackup mb = new MySqlBackup(cmd))
                        {
                            cmd.Connection = conn;
                            conn.Open();
                            mb.ExportInfo.AddCreateDatabase = true;

                            mb.ExportInfo.AddDropTable = true;
                            mb.ExportInfo.ExportEvents = true;
                            mb.ExportInfo.ExportFunctions = true;
                            mb.ExportInfo.ExportProcedures = true;
                            mb.ExportInfo.ExportRoutinesWithoutDefiner = true;
                            mb.ExportInfo.ExportRows = true;
                            mb.ExportInfo.ExportTableStructure = true;
                            mb.ExportInfo.ExportTriggers = true;
                            mb.ExportInfo.ExportViews = true;
                            mb.ExportToFile(file);
                            conn.Close();
                            WriteToFile("Database Successfuly Backed Up");
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                WriteToFile(ex.ToString());
            }
            
        }


        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
         
            //BackupDatabase();
            //timer1.Stop();
            //timer1.Start();
        }


        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + @"\Logs\ServiceLog.txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message + " [" + DateTime.Now.ToString("MM/dd/yyyy hh:mm tt") + "]");
                }
            }
            else
            {
                string tempfile = Path.GetTempFileName();
                using (var writer = new FileStream(tempfile, FileMode.Create))
                using (var reader = new FileStream(filepath, FileMode.Open))
                {
                    var stringBytes = Encoding.UTF8.GetBytes(Message + " [" + DateTime.Now.ToString("MM/dd/yyyy hh:mm tt") + "]"  + Environment.NewLine);
                    writer.Write(stringBytes, 0, stringBytes.Length);

                    reader.CopyTo(writer);
                }
                File.Copy(tempfile, filepath, true);
                File.Delete(tempfile);
            }
        }



            //////
        }
}
